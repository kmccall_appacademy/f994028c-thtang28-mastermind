class Code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(guess)
    array = guess.upcase.chars
    colors = PEGS.keys
    raise unless array.length == 4
    array.all? { |ch| colors.include?(ch) } ? Code.new(array) :
    (raise "Invalid Entry")
  end

  def self.random
    color_code = []
    4.times { color_code << PEGS.keys.sample}
    Code.new(color_code)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(guess)
    count = 0
    (0..3).each { |idx| count += 1 if guess[idx] == self[idx]}
    count
  end

  def near_matches(guess)
    count = 0
    hsh = Hash.new(0)
    hsh2 = Hash.new(0)
    self.pegs.each {|ch| hsh[ch] += 1 }
    guess.pegs.each { |ch| hsh2[ch] += 1}
    guess.pegs.uniq.each do |ch|
      count += [hsh[ch], hsh2[ch]].min if hsh.include?(ch)
    end
    count - exact_matches(guess)
  end

  def ==(guess)
    return false unless guess.is_a? Code
    self.pegs == guess.pegs
  end

  PEGS = {
    "R" => "red",
    "G" => "green",
    "B" => "blue",
    "Y" => "yellow",
    "O" => "orange",
    "P" => "purple",
  }

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "What is your guess?"
    guess = gets.chomp
    while true
      begin
        Code.parse(guess)
        break
      rescue
        puts "Invalid Entry. What is your guess?"
        guess = gets.chomp
      end
    end
    Code.parse(guess)
  end

  def display_matches(guess)
    puts "#{secret_code.exact_matches(guess)} exact matches."
    puts "#{secret_code.near_matches(guess)} near matches."
  end

  def play
    system "clear"
    puts "Welcome to Mastermind!"
    puts "You have 10 turns to guess the right 4 color combination!"
    puts "The colors are (red, green, blue, yellow, orange, purple)"
    puts "You should type 'r' for red, 'g' for green, 'b' for blue etc."
    puts "Example: rgby"
    puts "Goodluck!"

    turns = 0

    until turns == 10
      guess = get_guess
      if guess == @secret_code
        puts "You Win!"
        break
      else
        display_matches(guess)
        turns += 1
        puts "You have #{10 - turns} turns left."
      end
    end

    if turns == 10
      puts "Game Over!"
      puts "The answer was #{@secret_code.pegs.join}."
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
